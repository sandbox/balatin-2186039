Integrates Drupal user functionality with standard MailChimp features such as
creating and editing campaigns and subscriber lists.

For details on using this module, visit the help page at
/admin/help/mailchimp_users.