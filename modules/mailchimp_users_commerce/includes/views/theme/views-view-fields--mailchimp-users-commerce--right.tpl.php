<td valign="top" style="font-size: 12px; line-height: 20px; font-family: Arial, sans-serif; color: #666666;">
  <h4 style="font-size: 14px; font-family: Arial, sans-serif; color: #000000;">
    <span style="color: #000000;"><?php print $fields['title']->content; ?></span>
  </h4>

  <ul style="list-style-position: inside; font-size: 12px; font-family: Arial, sans-serif; line-height: 20px; color: #666666; list-style-type: disc; padding-left: 6px; margin-left: 0;">
    <?php print strip_tags($fields['body']->content, '<li>'); ?>
  </ul>

  <h3 style="font-size: 16px; font-family: Arial, sans-serif; color: #000000;">
    <span style="color: #000000;"><span class="silence" style="font-weight: 400; color: #666666;">only</span> <span class="shout" style="color: #FF0000;"><?php print $fields['commerce_price']->content; ?></span></span>
  </h3>

</td>

<td width="180"><?php print $fields['field_image']->content; ?></td>