<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<tr>
  <?php if (isset($rows)): ?>
    <?php print($rows); ?>
  <?php endif; ?>
</tr>