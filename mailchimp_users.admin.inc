<?php
/**
 * @file
 * mailchimp_users admin settings form.
 */

/**
 * Generates the Mailchimp Users system settings form.
 */
function mailchimp_users_system_settings_form($form, &$form_state) {
  $form = array();

  $form['sync'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sync Drupal with Mailchimp'),
  );

  $form['sync']['mailchimp_sync'] = array(
    '#type' => 'button',
    '#value' => t('Sync'),
    '#validate' => array('mailchimp_users_sync'),
  );

  $form['config_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('MailChimp Users Configuration'),
  );

  $form['config_wrapper']['mc_users_list_id'] = array(
    '#type' => 'textfield',
    '#title' => t('List ID'),
    '#default_value' => variable_get('mc_users_list_id', ''),
    '#description' => t('MailChimp Users utilizes one main list and builds segments for individual user "lists".'),
    '#required' => TRUE,
  );

  $form['config_wrapper']['mc_users_filter_formats'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide campaign textarea filter formats.'),
    '#default_value' => variable_get('mc_users_filter_formats', 1),
  );

  return system_settings_form($form);
}
